DOCKER-SYMFONY-PSL
========================

This repository contains image composer for PHP + Nginx + MySQL , its purpose is to work with PSL's Symfony projects

Requirement
-------------
- Docker 

Token
-------
The ENV contains variables for the MySQL image settings (root,user,pwd) 

- [DATABASE_NAME]: Original test db
- [DATABASE_USER] : Database User
- [DATABASE_PASSWORD] : Database Password
- [DATABASE_ROOT_PASSWORD] : Database Root password

Installation
--------------

Here is some steps you need to perform.

- Clone it from GIT
- Inside /app folder clone your project(s)

- From your host machine folder root (CMD), build and start the containers with
```sh
docker-compose up -d --build 
```

- If you need SQL Server connections additionally you need to install odbc sql (this is for Hopper), this is done inside the CLI of PHP container
```sh 
curl -O https://download.microsoft.com/download/e/4/e/e4e67866-dffd-428c-aac7-8d28ddafb39b/msodbcsql17_17.7.2.1-1_amd64.apk
curl -O https://download.microsoft.com/download/e/4/e/e4e67866-dffd-428c-aac7-8d28ddafb39b/mssql-tools_17.7.1.1-1_amd64.apk
apk add --allow-untrusted msodbcsql17_17.7.2.1-1_amd64.apk 
apk add --allow-untrusted mssql-tools_17.7.1.1-1_amd64.apk 
```
- Copy the php.ini for PHP, this is also done inside the CLI of PHP container
```sh
/usr/local/etc/php cp php.ini-development php.ini
```



App projects and Nginx
-------
Inside "/docker/nginx/sites/default.conf" you'll find server definition for each of your projects, right now you need to specify a different port for each one. Whithin the cointainer you'll find it in "/var/www/"

